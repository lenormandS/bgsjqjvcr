var express = require('express');
var http = require('http');
var app = express();
var server = http.createServer(app);
var io = require('socket.io').listen(server);
server.listen(3000);

app.use(express.static(__dirname + '/public'));

app.get('/',function(req,res){
  res.send(__dirname + '/index.html');
})

io.on('connection',function(socket){
  console.log('connexion d\'un utilisateur')
})
